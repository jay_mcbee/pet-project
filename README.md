# Pet Project #



### What is this repository for? ###
*Uses the petfinder API to allow user to search for pets

### How do I get set up? ###
-fork and clone down to your machine

-cd into the project

-execute npm install from terminal

-execute bower install from terminal

-execute npm test to run tests

-execute npm start to start server 

--->should be up in the browser at localhost:3000
### Contribution guidelines ###
*You don't want to contribute to this

###Questions/Concerns ###
jmcbee1@gmail.com