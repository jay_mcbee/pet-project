describe('ApiFactory', function () {

  var api;
  beforeEach(module('petApp'));
  beforeEach(inject(function (_ApiUtils_) {
    api = _ApiUtils_;
  }));
   
  describe('getPet', function () {
    it('should call petfinder API and return promise', function () {
      var result = api.getPet('12121212');
      expect(result).to.have.property('success');
      expect(result).to.have.property('error');
    });
  });

  describe('getRandom', function () {
    it('should call petfinder API and return promise', function () {
      var result = api.getRandom();
      expect(result).to.have.property('success');
      expect(result).to.have.property('error');
    });
  });

   describe('findPet', function () {
    it('should call petfinder API and return promise', function () {
      var mockedForm ={
      	animal: 'dog',
      	breed: 'welsh corgi',
      	location:{
      		city: 'Austin',
      		state: 'Texas'
      	}
      }
      var result = api.findPet(mockedForm);
      expect(result).to.have.property('success');
      expect(result).to.have.property('error');
    });
  });

  describe('listBreeds', function () {
    it('should call petfinder API and return promise', function () {
      var type = 'Dog'
      var result = api.listBreeds(type);
      expect(result).to.have.property('success');
      expect(result).to.have.property('error');
    });
  });
})