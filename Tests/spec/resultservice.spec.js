describe('resultservice', function () {

  var resServe;
  beforeEach(module('petApp'));
  beforeEach(inject(function (_ResService_) {
    resServe = _ResService_;
  }));
  console.log(resServe);
  describe('getNumOfPages', function () {

    it('creates and returns array of page objects', function () {
      var pages = resServe.getNumOfPages(180);
      console.log(pages)
      expect(pages).to.be.an('array');
      expect(pages[0]).to.have.property('page', 1);
      expect(pages[0]).to.have.property('first', 0);
      expect(pages[0]).to.have.property('last', 18);
      expect(pages.length).to.equal(10);
    });

  });

  describe('sortLToS', function(){
    it('sorts array of objects with property size from largest to smallest', function(){
      var arr = [{size:{$t:'S'},name:'Puff'},{size:{$t:'M'},name:'Charlie'},{size:{$t:'L'},name:'Biscuit'}]

      var sorted = resServe.sortLToS(arr);
      console.log(sorted)
      expect(sorted[0].name).to.equal('Biscuit');
      expect(sorted[1].name).to.equal('Charlie');
      expect(sorted[2].name).to.equal('Puff');
    })
  });

  describe('sortSToL', function(){
    it('sorts array of objects with property size from smallest to largest', function(){
      var arr = [{size:{$t:'S'},name:'Puff'},{size:{$t:'M'},name:'Charlie'},{size:{$t:'L'},name:'Biscuit'}].reverse();

      var sorted = resServe.sortSToL(arr);

      expect(sorted[0].name).to.equal('Puff');
      expect(sorted[1].name).to.equal('Charlie');
      expect(sorted[2].name).to.equal('Biscuit');
    })
  });

  describe('turnPage', function(){
    it('selects next or previous slice of results array and filters undefined', function(){
      var context = {
        currentPage:null,
        searchResults:[1,2,3,4,5,6,7]
      }
      var pgObj = {
        first: 0,
        last: 3
      }
      resServe.turnPage(pgObj,context);
      expect(context.currentPage).to.be.an('array');
      expect(context.currentPage[0]).to.equal(1);
      expect(context.currentPage[2]).to.equal(3);
      expect(context.currentPage.length).to.equal(3);
    })
  });

});