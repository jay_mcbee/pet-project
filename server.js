var express = require('express');
var bodyParser = require('body-parser');

var app = express();




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use(express.static(__dirname + '/public'));


require('./routes')(app);

var port = process.env.PORT || 3000; 

app.listen(port);

console.log('Up on port ' + port);

exports = module.exports = app;


