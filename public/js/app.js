angular.module('petApp',[
	'ui.router', 
	'petApp.APIRequests',
	'petApp.PetFormService',
	'petApp.SearchForm',
	'petApp.ResultsCtrl',
	'petApp.ResultsService',
	'petApp.AlternateSearchController'
	])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

	// $urlRouterProvider.otherwise('/');
	$stateProvider
	
	.state('home',{
		url: '/',
		templateUrl: 'views/index.html',
	})
	
	.state('pets',{
		url: '/pets',
		templateUrl: 'views/pets.html',
		controller: 'FormController',
	})

	.state('results',{
		url: '/searchResults',
		templateUrl: 'views/pets.results.html',
		controller: 'ResController'
	})

	.state('random',{
		url: '/random',
		templateUrl: 'views/randomSpecific.html',
		controller: 'AlternateController'
	})
	
}]);