angular.module('petApp.ResultsCtrl', [ 'petApp.ResultsService'])

.controller( 'ResController', [
	'$scope', 
	'$rootScope', 
	'ResService',
	'$location',
	'$anchorScroll',
	'$state', 
	function($scope, $rootScope, ResService, $location, $anchorScroll, $state){

	$scope.button = 'Back to Results';

	$scope.currentPage = 0;
  
  //next is called on line 39--> -1 will update to the zero index
	$scope.bookmark = -1;
  
  //Grab searchResults from rootScope
	$scope.searchResults = $rootScope.searchResults;
  
  //Create page objects
  $scope.pagination = ResService.getNumOfPages($scope.searchResults.length);
  console.log($scope.pagination);
  //Pet selected by user
	$scope.fullProfile = null;

	$scope.redirectToSearch = function(){
		$state.go('pets');
	}

	$scope.sortLargeToSmall = function(){
		$scope.currentPage = ResService.sortLToS($scope.currentPage);
	}

	$scope.sortSmallToLarge = function(){
		$scope.currentPage = ResService.sortSToL($scope.currentPage);
	}
 
	$scope.next = function(idx){
    ResService.turnPage($scope.pagination[idx + 1], $scope);
    $location.hash('top')
    $anchorScroll()
    $scope.bookmark++;
	}

	$scope.previous = function(idx){
		ResService.turnPage($scope.pagination[idx - 1], $scope);
		$location.hash('top')
    $anchorScroll()
		$scope.bookmark--;
	}
  
  //Grabs specific pet from results
	$scope.showProfile = function(idx){
  	$scope.fullProfile = $scope.currentPage[idx];
  	console.log($scope.fullProfile);
  }
  
  //Closes modal containing full profile
  $scope.closeModal = function(){
  	$scope.fullProfile = null;
  }

  $scope.next(-1);

}])

.directive('sortRedirect', function(){
	return{
	  restrict : 'E',
	  scope:{
			'sToL' : '&smallLarge',
			'lToS' : '&largeSmall',
			'redirect' : '&redirectTo'
	  },
	  templateUrl: '/views/sort-redirect.html'
	}
})

.directive('pictureGrid', function(){
	return {
		restrict: 'E',
		scope: {
			'show' : '&showInfo',
			'current' : '=',
		},
		templateUrl: '/views/result-grid.html'
	}
})

.directive('paginationButton', function() {
	return {
		restrict: 'E',
		scope: {
			'next': '&nextPage',
			'previous': '&previousPage',
			'currentPage' : '=page',
			'totalPages' : '=chapters'
		},
		templateUrl: '/views/pagination-buttons.html'
	}
})

.directive('profileInfo', function(){
	return{
	  restrict: 'E',
	  scope: {
	  	'button' : '=',
	  	'result' : '=resultProfile',
	  	'redirect' : '&redirectTo'
	  },
	  templateUrl: '/views/profilemodal.html'
	}
})


