angular.module('petApp.SearchForm', [ 'petApp.PetFormService', 'petApp.APIRequests' ])

.controller('FormController', [ 
	'$scope', 
	'$rootScope', 
	'$state', 
	'PetForm', 
	'ApiUtils', 

	function( $scope, $rootScope, $state, PetForm, ApiUtils ){

	$scope.animals = PetForm.animals;

	$scope.breeds = null;
  
  $scope.states = PetForm.states;

  $scope.notFound = null;

  $scope.noResults = null;

  $scope.fetching = false;

  //$rootScope makes these results available to all controllers

  $rootScope.searchResults = null;

  $rootScope.randOrSpecResult = null;
	
	$scope.formData = {
		animal: null,
		breed: null,
		location:{
			city: null,
			state: null
		}
  }

  $scope.idSearch = {
  	id: null
  }
  
  //$state.go convenience method for redirects
  $scope.redirectToResults = function(){
    $state.go('results')
  }

  $scope.redirectToRandom = function(){
    $state.go('random')
  }

  //functions from line 51 to 84 unwrap the promises from the APIfactory

  $scope.getRandom = function(){
  	ApiUtils.getRandom()
  	  .then( function(resp) {
  	  	$rootScope.randOrSpecResult = resp.data.petfinder.pet;
  	  	$scope.redirectToRandom();
  	  })
  	  .catch( function(err){console.log(err)})
  }

  $scope.getById = function(id){
  	console.log('id in getby', id)
  	ApiUtils.getPet(id)
  	  .then( function(resp) {
  	  	if(resp.data.petfinder.header.status.code.$t === '201'){
  	  		$scope.notFound = resp.data.petfinder.header.status.message.$t;
  	  	}else{
  	  	  $rootScope.randOrSpecResult = resp.data.petfinder.pet;
  	      $scope.redirectToRandom();
  	    }
  	  })
  	  .catch( function(err){console.log(err)})
  }
  
  //call to get breed from DB-->unrwraps the promise from the API util PetForm.getBreeds(type)
	$scope.fetchBreeds = function(){
		var type = $scope.formData.animal;
		ApiUtils.listBreeds(type)
		  .then( function(resp) {
		  	$scope.breeds = resp.data.petfinder.breeds.breed;
	    })
		  .catch( function(err){ console.log(err)})
	}

	$scope.search = function(formInfo){
		$scope.fetching = true;
		ApiUtils.findPet(formInfo)
		  .then( function(resp) {
      	$rootScope.searchResults = resp.data.petfinder.pets.pet;
	  	  $scope.fetching = false;
	  	  $scope.redirectToResults();
		  })
		  .catch( function(err){ console.log(err)})
	}

}])