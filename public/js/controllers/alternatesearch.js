angular.module('petApp.AlternateSearchController', [])

.controller( 'AlternateController', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state){

	$scope.alternateRes = $rootScope.randOrSpecResult;

	$scope.buttonMessage = 'New Search';

	$scope.redirectToSearch = function(){
    $state.go('pets')
	}
}])

.directive('modalProfile', function(){
	return{
	  restrict: 'E',
	  scope: {
	  	'button' : '=buttonText',
	  	'result' : '=resultProfile',
	  	'redirect' : '&redirectTo'
	  },
	  templateUrl: '/views/profilemodal.html'
	}
})

