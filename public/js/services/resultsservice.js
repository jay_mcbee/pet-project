angular.module('petApp.ResultsService', [])

.service('ResService', function(){

	
	
		//calculate the total number of pages from potentially 180 results
		function getNumOfPages(total){
	    var pages = [], count = Math.floor(total/18), start = 1;
      var firstIdx = 0;
      var lastIdx = 18;
      if(count < 1){
      	pages.push({
      		page: 1,
      		first: 0,
      		last: total
      	})
      	return pages;
      }else{
        while(start <= count){
    	    pages.push({
    		    page: start,
    		    first: firstIdx,
    		    last: lastIdx
    	    });
    	    start++;
    	    firstIdx = lastIdx;
    	    lastIdx+=18;
    	  }
      }
      return pages;
	  }

	  function sortLToS(array){
	  	var corollary = {
	  		S:0,
	  		M:1,
	  		L:2,
	  		XL:3,
	  	}
	  	return array.sort( function(a,b) {
	  		return corollary[b.size.$t] - corollary[a.size.$t]
	  	})
	  }

	  function sortSToL(array){
	  	var corollary = {
	  		S:0,
	  		M:1,
	  		L:2,
	  		XL:3,
	  	}
	  	return array.sort( function(a,b){
	  		return corollary[a.size.$t] - corollary[b.size.$t]
	  	})
	  }
    
    //Goes to next or previous page 
	  function turnPage(pgObj, context){
	  	context.currentPage = context.searchResults
	  	  .slice(pgObj.first, pgObj.last)
	  	  .filter(function(animal){ return animal !== undefined})
	  }
   
	
	return{
		getNumOfPages : getNumOfPages,
		sortLToS : sortLToS,
		sortSToL : sortSToL,
		turnPage : turnPage,
	}
})
