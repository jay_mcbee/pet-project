angular.module('petApp.PetFormService', [])

.service('PetForm', ['$http', function( $http ){
  
	var animals = ['barnyard', 'bird', 'cat', 'dog', 'horse', 'pig', 'reptile', 'smallfurry'];

	var states = ['AL','AK','AZ','AR','CA','CO','CT','DE',
	'FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME',
	'MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ',
	'NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD',
	'TN','TX','UT','VT','VA','WA','WV','WI','WY']


  return{
  	animals: animals,
  	states: states,
  }
}])