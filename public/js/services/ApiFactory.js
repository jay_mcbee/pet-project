'use strict'

angular.module('petApp.APIRequests', [])

.factory('ApiUtils', ['$http', function($http){


	var key = 'f730142aff1e2e4d62a25aa89618a62c';

  var hash = '33df8a3dc23a3517bbfa16291218d36f'

  var base = 'http://api.petfinder.com/';
  var json = '&format=json&callback=JSON_CALLBACK';
  var count = '&count=180';
    //the angular http client is promisified so all requests return unresolved promises
    
    //fetches auth token to make subsequent requests.
    // var getAuthToken = function(){
    //   var reqUrl = 'http://api.petfinder.com/auth.getToken?key='+key+'&sig='+hash+json;
    // 	return $http.jsonp(reqUrl)
    // }
  
    //fetches specific pet based on user input
    var getPet = function(input){
      var reqUrl = base +'pet.get?key='+key+'&id='+input+json
    	return $http.jsonp(reqUrl);
    }
    
    //fetches pet w/out user input
    var getRandom = function(){
      var reqUrl = base+'pet.getRandom?key='+key+'&output=full'+json
    	return $http.jsonp(reqUrl)
    }
    
    //fetches pets based on user search criteria
    var findPet = function(input){
      var animal = input.animal;
      var breed = input.breed;
      var city = input.location.city.toLowerCase();
      var state = input.location.state.toLowerCase();
      
      var reqUrl = breed ?
         base+'pet.find?key='+key+'&animal='+animal+'&breed='+breed.$t+'&location='+city+','+state+json+count 
        :base+'pet.find?key='+key+'&animal='+animal+'&location='+city+','+state+json+count
        
        return $http.jsonp(reqUrl);
    }
    
    //fetches breeds for specific animal
    var listBreeds = function(type){
      var reqUrl = base + 'breed.list' + '?key=' + key + '&animal=' + type + json;
      return $http.jsonp(reqUrl)
    }
    

  return {
    // getAuth : getAuthToken,
    listBreeds: listBreeds,
    getPet : getPet,
    getRandom : getRandom,
    findPet : findPet,
  }

}])